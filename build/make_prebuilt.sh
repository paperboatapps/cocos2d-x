# define output folder environment variable
BUILD_DIR=../build
BUILD_ROOT=../build
#BUILT_PRODUCTS_DIR=.
UNIVERSAL_OUTPUTFOLDER=../prebuilt/ios

CONFIGURATION=Release

# Build Device version
xcodebuild -target libcocos2d-x ONLY_ACTIVE_ARCH=NO BITCODE_GENERATION_MODE=bitcode -configuration ${CONFIGURATION} -sdk iphoneos  BUILD_DIR="${BUILD_DIR}" BUILD_ROOT="${BUILD_ROOT}"

# make sure the output directory exists
mkdir -p "${UNIVERSAL_OUTPUTFOLDER}"

# Step 2. Create universal binary file using lipo
lipo -create -output "${UNIVERSAL_OUTPUTFOLDER}/libcocos2d-x.a" "${BUILD_DIR}/${CONFIGURATION}-iphoneos/libcocos2d-x.a"

# remove the build files
rm -rf "${BUILD_DIR}/${CONFIGURATION}-iphoneos"

# strip symbols
xcrun -sdk iphoneos strip -S "${UNIVERSAL_OUTPUTFOLDER}/libcocos2d-x.a"
